import React from "react";
import PropTypes from "prop-types";
import "../modules/css/IndexList.css";

function ListItem(props) {
  return <div>{props.animal}</div>;
}

function List(props) {
  return (
    <div>
      {props.animal.map((animal, daddy) => {
        return <ListItem key={daddy} animal={animal} />;
      })}
    </div>
  );
}

function RenderingListOfComponents() {
  const animals = ["cat", "dog", "fish"];
  return (
    <div>
      <h2>Rendering Animal List</h2>
      <div className="mx-auto bg-info d-inline-block">
        {List({ animal: animals })}
      </div>
    </div>
  );
}
RenderingListOfComponents.propTypes = {};

export default RenderingListOfComponents;
