import React from "react";
import "../modules/css/IndexList.css";

// function getFullNames(props) {
//   const people = [
//     { firstname: "Malcom", lastname: "Reynolds" },
//     { firstname: "Kaylee", lastname: "Frye" },
//     { firstname: "Jayne", lastname: "Cobb" },
//   ];

//   return (
//     <ul>
//       {people.map((item) => (
//         <li>{[item.firstname, item.lastname].join(" ")}</li>
//       ))}
//     </ul>
//   );
// }

function animal() {
  const animals = ["cat", "dog", "fish"];
  const animalsList = animals.map((animal, pets) => (
    <li>
      {" "}
      key{pets} {animal}
    </li>
  ));
  return (
    <div>
      <h1> Animal: </h1>
      <ul>
        <li>{animalsList}</li>
      </ul>
    </div>
  );
}

export default animal;
// export default getFullNames;
