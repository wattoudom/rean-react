import React from "react";
import "../modules/css/IndexList.css";

function FilterAnimal(props) {
  return (
    <div>
      {props.animals.map(function (animal) {
        return animal.startsWith("l") ? animal : null;
      })}
    </div>
  );
}

function FilterList() {
  const animals = ["cat", "dog", "fish"];
  return (
    <div>
      <h1>Animals: </h1>
      <FilterAnimal animals={animals} />
    </div>
  );
}

export default FilterList;
